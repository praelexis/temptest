from hello import greet, farewell

import time

if __name__ == "__main__":
    name = input("What is your name?  ")
    name = name.replace('\\n', '\n')
    print(greet(name))
    print("This is where the magic happens")
    print("Close your eyes")
    time.sleep(1)
    print("Open them")
    print("If you read that, you cheated")
    print(farewell(name))
    print("Stuff")
    print("Stuff 1")
    print("Stuff 2")
    print("Stuff 3")
    print("Stuff 4")
    print("Stuff 5")
