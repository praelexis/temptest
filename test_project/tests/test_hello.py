# Software Copyright Notice
# Copyright 2021 Praelexis (Pty) Ltd.
# All rights are reserved
#
# Copyright exists in this computer program and it is protected by
# copyright law and by international treaties. The unauthorised use,
# reproduction or distribution of this computer program constitute acts
# of copyright infringement and may result in civil and criminal
# penalties. Any infringement will be prosecuted to the maximum extent
# possible.
#
# Praelexis (Pty) Ltd chooses the following address for delivery of all
# legal proceedings and notices:
#    Capital Place F,
#    15-21 Neutron Avenue,
#    Stellenbosch,
#    7600,
#    South Africa.

"""Simple tests for the test module"""

from unittest import TestCase

from ..hello import greet


class TestHello(TestCase):
    """Simplistic tests"""

    def test_paul_greet(self):
        """Test 1"""
        self.assertEqual(greet("Paul"), "Hello Paul")

    def test_ringo_greet(self):
        """Test 2"""
        self.assertEqual(greet("Ringo"), "Hello Ringo")
