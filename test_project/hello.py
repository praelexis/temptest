# Software Copyright Notice
# Copyright 2021 Praelexis (Pty) Ltd.
# All rights are reserved
#
# Copyright exists in this computer program and it is protected by
# copyright law and by international treaties. The unauthorised use,
# reproduction or distribution of this computer program constitute acts
# of copyright infringement and may result in civil and criminal
# penalties. Any infringement will be prosecuted to the maximum extent
# possible.
#
# Praelexis (Pty) Ltd chooses the following address for delivery of all
# legal proceedings and notices:
#    Capital Place F,
#    15-21 Neutron Avenue,
#    Stellenbosch,
#    7600,
#    South Africa.

"""The implementation"""


def greet(name):
    """Greet someone"""
    return 'Hello %s' % name


def farewell(name):
    """Say goodbye"""
    data = 'result = "Go Well %s"' % name
    gob = {}
    exec(data, gob)
    return gob['result']
