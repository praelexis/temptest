# Required imports
from setuptools import setup, find_packages
import os
import platform
import sys

with open('README.md', 'r', encoding='utf-8') as readme_file:
    readme = readme_file.read()

# Install
setup(name='test_project',
      version='0.0.0.0',
      description='Test project for pipelines stuff',
      long_description=readme,
      long_description_content_type='text/markdown',
      keywords='Do not use',
      url='http://www.praelexis.com/',
      author='Praelexis',
      author_email='admin@praelexis.com',
      license='PROPRIETARY',
      # This installs to some location like /usr/bin, but most likely <ANACONDA>/bin (it depends on the environment).
      packages=find_packages(),
      install_requires='\n'.join(requirements),
      include_package_data=True,
      zip_safe=False)
